import React, {Component, Fragment} from 'react';
import Table from './table';

class UsersData extends Component{
    constructor(props){
        super(props);

        this.state = {
            people: [
                { id: 1, firstName: "Jorge", lastName: "Corea", age: 25 },
                { id: 2, firstName: "Nick", lastName: "Rueda", age: 30},
                { id: 3, firstName: "Luis", lastName: "Fajardo", age: 20},
                { id: 4, firstName: "Engel", lastName:"Pena", age:20}
            ]
        }
    }

    addUser = user => {
        this.setState(prevState => {
            return {
                people: [...this.state.people, user]
            }
        });
    }

    removeUser = id => {
        const filtered = this.state.people.filter(item => item.id !== id);
        this.setState( _ => {
            return {
                people: [...filtered]
            }
        })
    }

    render(){
        return(
            <Fragment>
                <Table 
                    title="Titulo de la tabla"
                    subtitle="Subtitulo de esto"
                    list={this.state.people}
                    addUser={this.addUser}
                    removeUser={this.removeUser}
                />
            </Fragment>
        )
    }
}

export default UsersData;

