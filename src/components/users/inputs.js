import React, {Component, Fragment} from 'react';

class Input extends Component {
    constructor(props){
        super(props);

        this.state = {
            firstName:"Jorge",
            lastName: "Corea",
            age: 25,
        }
    }

    handleChangeFirstName = ({target: {value}}) => {
        this.setState(prevState => {
            return {
                firstName: value
            }
        });
    }

    handleChangeLastName = ({target: {value}}) => {
        this.setState(prevState => {
            return{
                lastName: value
            }
        });
    }

    handleInputChangeAge = ({target: {value}}) => {
        this.setState(prevState => {
            return{
                age: value
            }
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        const user = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            age: this.state.age,
        }

        this.props.addUser(user);

        this.setState(prevState =>{
            return{
                firstName: "",
                lastName: "",
                age: 0
            }
        });
    }

    render(){
        return(
            <Fragment>
               <form onSubmit={this.handleSubmit} class="form-inline personForm">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" onChange={this.handleChangeFirstName} name="firstName" value={this.state.firstName}></input>

                    <label for="exampleInputEmail1">LastName</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" onChange={this.handleChangeLastName} name="lastName" value={this.state.lastName}></input>

                    <label for="exampleInputEmail1">Age</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" onChange={this.handleInputChangeAge} name="age" value={this.state.age}></input>

                    <button type="submit" class="btn btn-primary">Submit</button>
               </form>
            </Fragment>
        );
    }
}

export default Input;