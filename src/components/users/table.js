import React, {Component, Fragment} from 'react';
import AddPerson from "./inputs";

class Table extends Component{
    constructor(props) {
        super(props);

        this.state = {
            title: "Titulo con estado"
        }
    }

    handleRemove = id => {
        this.props.removeUser(id);
    }

    renderRow = _ => {
        return(
            <Fragment>
                {
                    this.props.list.map((person, index) => {
                        return (
                            <tr>
                                <th scope="row">{person.id}</th>
                                <td>{person.firstName}</td>
                                <td>{person.lastName}</td>
                                <td>{person.age}</td>
                                <td>
                                    <button onClick={_ => { this.handleRemove(person.id) }} className="btn btn-danger">
                                        Borrar
                                    </button>
                                </td>
                            </tr>
                        )
                    })
                }
            </Fragment>
        )
    }

    render(){
        return(
            <Fragment>
                <h1>{ this.state.title }</h1>
                <h2>{ this.props.subtitle }</h2>
                <AddPerson addUser={this.props.addUser} />
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Age</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.renderRow() }
                    </tbody>
                </table>
            </Fragment>
        );
    }
}

export default Table;