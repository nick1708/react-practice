import React from 'react';
// import logo from './logo.svg';
import './App.css';

import UsersData from './components/users/index';

function App() {
  return (
    <div className="App">
      <div className="container main-container">
        <UsersData />
      </div>
    </div>
  );
}

export default App;
